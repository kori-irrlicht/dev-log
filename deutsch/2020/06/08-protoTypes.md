---
author: Kori
title: "/dev/log 2: ProtoTypes"
tags: [Project OpenMonster]
date: 2020-06-08
---
# ProtoTypen
In der vergangenen Woche habe ich die initiale Version für das Typensystem in *Project OpenMonster* implementiert.
Das System folgt dabei bekannten Regeln: Jeder Angriff wird einem Typen zugeordnet, wie zum Beispiel "Feuer" oder "Eis". Jedes Monster kann bis zu zwei verschiedene Typen haben.

Den einzelnen Angreifer-Verteidiger-Typkombinationen können dabei verschieden große Effektivitätswerte zugewiesen werden. Das heißt, dass ein sehr effektiver Angriff nicht unbedingt um denselben Faktor gegen verschiedene Typen verstärkt wird. Möchte man also sagen, dass "Feuer" gegen "Eis" effektiv ist und "Feuer" gegen "Pflanze" effektiv ist, aber "Eis" dabei anfälliger ist als "Pflanze", so ist das mit diesem System möglich, indem man der Kombination "Feuer-Eis" einen Wert von 2 und der Kombination "Feuer-Pflanze" einen Wert von 3 zuweist.

Typen können im "data"-Ordner in der "types.json" eingetragen werden. Alternativ unterstützt das Promo-Tool das erstellen von Typen. Angreifer-Verteidiger-Typkombinationen müssen derzeit noch per Hand in der "types.json" eingetragen werden. 

Die aktuelle Implementierung erlaubt es theoretisch sogar Monster zu erschaffen, die mehr als zwei Typen haben. Ob dies spieltechnisch sinnvoll ist muss sich aber erst zeigen.

Derzeit sind die Typen wie folgt geplant:
 - Wasser
 - Feuer
 - Wald
 - Metall
 - Licht
 - Dunkel
 - Schatten
 - Eis
 - Gift
 - Erde
 - Elektro 
 - Luft
 - Verstand
 - Körper
 - Neutral

# Monsterinfo
Eine weitere neue Funktion ist das Abfragen detaillierter Monsterdaten durch ihren Besitzer. Bisher war es nur möglich, die LP und Ausdauerwerte als Prozentzahl abzufragen. Dies ist auch weiterhin möglich und steht allen Nutzern zur Verfügung. 
Aus strategischen Gründen sollen die Informationen die Gegner erfahren können minimiert werden. Deshalb werden die LP der Monster nur in Prozent angegeben. Der Besitzer der Monster soll aber in der Lage sein alle Informationen abrufen zu können. 

Über die allgemeine Abfrage ist es auch nicht möglich die Angriffe eines Monsters zu erfahren. Auch diese Informationen werden dem Besitzer nun über die neue Abfrage mitgeteilt.
Dadurch können im Testclient nun auch die Angriffe des eigenen Monster angezeigt werden.

# Starterauswahl
Zum Schluss: Seit dieser Woche ist es möglich eine Anfrage an den Server zu senden um eine Liste an möglichen Startermonstern zu erhalten. Bisher musste der Client im Voraus wissen, welche Starter zur Verfügung stehen.
Die Liste der Startermonster kann dabei auf Serverseite konfiguriert werden. Derzeit ist dies über die allgemeine Serverkonfiguration möglich. Diese Funktion wird aber in Zukunft in den "data" Ordner verschoben werden. 

