---
author: Kori
title: "/dev/log 1: Hello world!"
tags: [meta, Project OpenMonster, promo-tool, promo-msg-gen]
date: 2020-06-01
---

# Hello world!

Recently I've finished the basic versions of some of the tools ([Promo-Tool](https://gitlab.com/kori-irrlicht/promo-tool), [Message Generator](https://gitlab.com/kori-irrlicht/project-openmonster-message-generator)) I needed to work on [Project OpenMonster](https://gitlab.com/kori-irrlicht/project-open-monster).

Since my focus is going to return to the project itself, I thought: "This might be a good moment to start this devlog". 

Beginning with this post, I'd like to create the regular */dev/log* series in which I will write about the progress of *Project OpenMonster*. Being only a hobbyist gamedev, I hope to update the */dev/log* and the project itself regularly.

# What is Project OpenMonster?
*Project OpenMonster* itself is an open-source server for a monster collecting game.
I'm going to program a basic client with the [Godot engine](https://godotengine.org), but my main focus will be the server. 
By creating an open-source server and a comprehensible API I want to provide the community with the ability to create their own clients.
Last but not least, the prioritized operating systems are going to be recent versions of Linux and Windows.

The server will be mostly data-driven, so people can easily create new content or modify existing content like monsters, attacks or items.
I think this one is important. Allowing people to create their own content is what made games like Skyrim or Minecraft really popular.

So much to the technical side. Like I said before, *Project OpenMonster* is going to be a monster collecting game. 
The priority will be to create a fun battle system. Currently I'm planning a turn-based system, where each player commands two monsters simultaneously.
In later versions, each player will have their own dimension where they can breed and raise monsters. 

# What is the Promo-Tool?
The Promo-Tool (just in case: Promo is short for Project OpenMonster :) ) can be used to create the game content. Currently it supports the creation of:
 - monster species
 - items
 - moves

I'm quite lazy and I don't want to write the same JSON structure down for each monster, item or move. So I wrote the Promo-Tool, which takes care of the JSON structure and allows me to focus on the content itself.

# What is the Message Generator?
The Message Generator takes the API definitions of the server as an input and can generate the model and path constant for a given language (currently only Go and Gdscript are supported).
The generator can also generate methods to validate the model, which allows me to document the validation methods in the API definition. This also ensures that the clients and server use the same validation mechanics, if they generate the code from the API definition.

