---
author: Kori
title: "/dev/log 2: ProtoTypes"
tags: [Project OpenMonster]
date: 2020-06-08
---
# ProtoTypes
Last week I've finished the prototype for the typesystem in *Project OpenMonster*.
The system follows the known rules: Every Attack has a type, like "Fire" or "Ice". Every monster can have up to two different types.

Every attacker-defender-type combination can have different effectivity values. While an attack can be effective against two different types, the effectivity may not be the same. For example, if you use a "Fire"-attack against an "Ice"-monster and a "Forest"-monster, the "Ice"-monster might receive more damage then the "Forest"-monster.

Types can be declared in the "types.json" file in the "data" folder. Alternativly, the promo-tool supports the creation of types. Currently attacker-defender-type combinations must be manually added to the "types.json" file.

The current implementation even allows the creation of monster with more then two types. If this will be relevant for the gameplay must be determined in future tests.

Currently the following types are planned:
 - Water
 - Fire
 - Forest
 - Metal
 - Light
 - Dark
 - Shadow
 - Ice
 - Poison
 - Earth
 - Electric 
 - Air
 - Mind
 - Body
 - Neutral


# Monster data
Another new feature the requesting of detailed monster data by the owner. Before this change it was only possible to request the HP and Stamina in percent. This is still available and every user can use this to request monster data.
From a strategic view the opponent should receive as less information as possible. Because of this limitation other user can only receive the HP and Stamina in percent. But the owner of a monster should be able to receive more detailed information about their monsters.

Also it is not possible to receive the list of known moves through the common request. This information is necessary to the owner since the owner must choose an attack in a battle.

# Starter monsters
Since this week it is possible to request a list of monster from the server from which the user can choose their starter monster. Up to this point the client had to hardcode the possible monsters. But now the list of starters can be configured on the server. Currently this is possible in the server configuration, but in future commits it will be moved to the "data" folder.

